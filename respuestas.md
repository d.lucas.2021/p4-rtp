## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: La captura está compuesta por 1268 paquetes.
* ¿Cuánto tiempo dura la captura?: Dura 12.81 segundos. Esto se puede ver por ejemplo en estadísticas, en el apartado de propiedades del archivo de captura.
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 paquetes por segundo.
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N):224
* Dirección IP de la máquina A: 192.168.0.10
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete: 49154
* Puerto UDP hacia el que se envía el paquete: 54550
* SSRC del paquete: 0x2A173650
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5.
* ¿Cuántos paquetes hay en el flujo F?: 642.
* ¿El origen del flujo F es la máquina A o B?: Es la máquina A.
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550.
* ¿Cuántos segundos dura el flujo F?: 12.81 .
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 12.83 ms. 
* ¿Cuál es el jitter medio del flujo?: 12.23 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes perdidos
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 12.78 segundos
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 26640
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: Ha llegado pronto, ya que presenta un valor de skew negativo-
* ¿Qué jitter se ha calculado para ese paquete? 12.33 ms.
* ¿Qué timestamp tiene ese paquete? 17920.
* ¿Por qué número hexadecimal empieza sus datos de audio? 2.
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: mié 18 oct 2023 17:54:32  
* Número total de paquetes en la captura: 994
* Duración total de la captura (en segundos): 5.88 
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450
* ¿Cuál es el SSRC del flujo?: 0x32dbbf10
* ¿En qué momento (en ms) de la traza comienza el flujo? En el milisegundo 0
* ¿Qué número de secuencia tiene el primer paquete del flujo? 6439
* ¿Cuál es el jitter medio del flujo?: 0 segundos
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes perdidos
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: p.rivero.2021
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): 1551
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: mismos paquetes 
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: 25034-6439= 18595
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: los valores del ancho de banda, los numeros de secuencia y ligeras diferencias en los valores del campo delta
